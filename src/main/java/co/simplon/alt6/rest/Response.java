package co.simplon.alt6.rest;

import java.io.IOException;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

public class Response<T> {
    private T body;
    private int code = 200;

    public Response(T body, int code) {
        this.body = body;
        this.code = code;
    }
    public Response(T body) {
        this.body = body;
    }


    public void makeResponse(HttpExchange httpExchange) throws IOException {
        Gson gson = new Gson();
        String jsonBody = gson.toJson(body);
        httpExchange.getResponseHeaders().add("Content-Type", "application/json");
        httpExchange.getResponseHeaders().add("access-control-allow-method", "*");
        httpExchange.getResponseHeaders().add("access-control-allow-origin", "*");
        httpExchange.getResponseHeaders().add("referrer-policy", "no-referrer");

        httpExchange.getResponseHeaders().add("Server", "Java");
        httpExchange.getResponseHeaders().add("Content-Security-Policy", "default-src 'self'; connect-src http://localhost:8080");
        httpExchange.sendResponseHeaders(code, jsonBody.length());
        httpExchange.getResponseBody().write(jsonBody.getBytes());
        httpExchange.close();
    }
}
