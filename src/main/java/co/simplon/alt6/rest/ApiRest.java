package co.simplon.alt6.rest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.sun.net.httpserver.HttpServer;

import co.simplon.alt6.rest.controller.AbstractController;
import co.simplon.alt6.rest.controller.FirstController;
import co.simplon.alt6.rest.controller.PersonController;

public class ApiRest {
    private HttpServer server;
    private Map<String, AbstractController> routeControllers = new HashMap<>(
        Map.of(
            "/api/first", new FirstController(),
            "/api/person", new PersonController()
        )
    );

    public void start(int port) {

        try {
            server = HttpServer.create(new InetSocketAddress(port), 50);
            
            for (Entry<String, AbstractController> route : routeControllers.entrySet()) {
                server.createContext(route.getKey(), route.getValue());
                System.out.println("Route available : "+route.getKey()+" => "+route.getValue().getClass());
            }
            
            server.start();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
