package co.simplon.alt6.rest.controller;

import java.io.IOException;

import co.simplon.alt6.Person;
import co.simplon.alt6.rest.Response;
import co.simplon.alt6.rest.annotation.Route;

public class FirstController extends AbstractController {
    
    @Route("") // /api/first/route1
    public Response<String> route1() {

        return new Response<>("Salut");
    }

    @Route("/route2")
    public Response<Person> route2() throws IOException {
        
        return new Response<>(new Person("test", 10));
    }
}
