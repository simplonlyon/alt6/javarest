package co.simplon.alt6.rest.controller;

import co.simplon.alt6.Person;
import co.simplon.alt6.rest.Response;
import co.simplon.alt6.rest.annotation.Route;

public class PersonController  extends AbstractController{
    
    @Route(value = "/", methods = {"GET"})
    public Response<Person> getPerson() {
        return new Response<>(new Person("Jon", 46));
    }

    @Route(value = "/", methods = {"POST"})
    public Response<Person> postPerson(Person person) {
        System.out.println(person.getName());
        return new Response<>(person);
    }
}
