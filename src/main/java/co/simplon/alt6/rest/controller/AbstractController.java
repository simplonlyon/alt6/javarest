package co.simplon.alt6.rest.controller;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import co.simplon.alt6.Person;
import co.simplon.alt6.rest.Response;
import co.simplon.alt6.rest.annotation.Route;

public abstract class AbstractController implements HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String prefix = httpExchange.getHttpContext().getPath();
        String httpMethod = httpExchange.getRequestMethod();

        Gson gson = new Gson();

        String route = removeTrailingSlash(httpExchange.getRequestURI().getRawPath());

        for (Method method : getClass().getMethods()) {
            Route annotationRoute = method.getAnnotation(Route.class);
            if (annotationRoute != null) {
                String routeFromAnnotation = removeTrailingSlash(annotationRoute.value());
                
                if (route.equals(prefix + routeFromAnnotation) 
                    && Arrays.asList(annotationRoute.methods()).contains(httpMethod)) {
                    try {
                        Object parameter = null;
                        if(!httpMethod.equals("GET") && !httpMethod.equals("DELETE")) {
                            if(method.getParameterCount() == 1) {
                                parameter = gson.fromJson(new InputStreamReader(httpExchange.getRequestBody()), method.getParameterTypes()[0]);

                            }
                        }

                        if (method.invoke(this, parameter) instanceof Response response) {
                            response.makeResponse(httpExchange);
                        }
                        return;
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                        new Response<String>("Server Error", 500).makeResponse(httpExchange);

                        e.printStackTrace();
                    }
                }
            }
        }
        new Response<String>("Not Found", 404).makeResponse(httpExchange);

    }

    private String removeTrailingSlash(String route) {
        if (route.endsWith("/")) {
            route = route.substring(0, route.length() - 1);
        }
        return route;
    }

}
