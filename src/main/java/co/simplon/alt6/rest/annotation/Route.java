package co.simplon.alt6.rest.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Route {

    String value();
    String[] methods() default {"GET", "POST", "PUT", "PATCH", "DELETE"};
    
}
